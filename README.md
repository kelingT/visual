# 《旅行轨迹》

# 一、竞品分析
## 1.1 架构层
更据我们团队初步的构想，以‘pott’app作为竞品分析对象。‘pott’是一款在记录旅行打卡较为成熟的软件，有广大的地图标记，更据客户的旅行，标记分享旅行，以下为相应的内容分析。
### 主导航的信息架构简要列明：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0513/013650_a7bbc87a_3043318.png "微信图片_20200513013407.png")
### 细分页面的结构框架：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0513/013714_a483aa53_3043318.png "微信图片_20200513013422.png")

## 1.2 交互路径分析（两张图）
### 竞品交互流程-（若图片模糊请看-> [链接](http://kelingt.gitee.io/link)）
![输入图片说明](https://images.gitee.com/uploads/images/2020/0514/091821_b7e13c3b_3043318.png "1.PNG")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0514/091837_ad459f38_3043318.png "2.PNG")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0514/091851_4ac314a9_3043318.png "3.PNG")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0514/091907_b6499b8f_3043318.png "2020-05-14_091514.png")
### 竞品对比分析亮点
![输入图片说明](https://images.gitee.com/uploads/images/2020/0514/002639_dab035cd_3043318.png "捕0.PNG")

## 1.3 视觉层面分析
![输入图片说明](https://images.gitee.com/uploads/images/2020/0513/190203_88d9ca6c_3043318.png "微信图片_20200513185216.png")


# 二、自己作品（根据上面格式大概书写）
## 2.1 架构层
旅行轨迹APP为用户提供了照片打卡与地标识别这两大功能，能够通过用户上传的照片进行旅行打卡，在地图上生成专属地图，以及为广大旅行爱好者提供地标识别，推荐周边好去处，是一款适合旅游爱好者的软件。
### 主导航的信息架构简要列明：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0513/013808_0e34689b_3043318.png "微信图片_20200513013441.png")
### 细分页面的结构框架：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0513/013957_9ceb0726_3043318.png "微信图片_20200513013448.png")

## 2.2 交互路径
### 交互流程图-（若图片模糊请看-> [链接](http://nfunm171061397.gitee.io/travel_track_interface)）
![输入图片说明](https://images.gitee.com/uploads/images/2020/0513/014054_6f4350ab_3043318.png "微信图片_20200513013523.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0513/014119_7b912df6_3043318.png "微信图片_20200513013528.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0513/014140_3839a3c9_3043318.png "微信图片_20200513013533.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0513/014210_2d12c116_3043318.png "微信图片_20200513013540.png")


### 交互亮点
![输入图片说明](https://images.gitee.com/uploads/images/2020/0513/014232_17ad3bb7_3043318.jpeg "微信图片_20200513013516.jpg")
## 2.3 视觉规范
![输入图片说明](https://images.gitee.com/uploads/images/2020/0513/014301_7e7286a6_3043318.png "微信图片_20200513013436.png")
